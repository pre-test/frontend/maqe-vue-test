const express = require('express')
const Http = require('../../http')

const router = express()
// ประเภทกล่อง
router.get('/catalog/all-cats-brands', (req, res) => {
  Http.get('/catalog/allcatsbrands')
    .then((response) => {
      res.send({ status: 200, data: response.data})
    })
    .catch((e) => {
      res.status(404)
      res.send({ state: 404, messages: e.message })
    })
})

// สินค้าต่างๆ
router.get('/catalog/products', (req, res) => {
  Http.get('/catalog/filter', { params: req.query })
    .then((response) => {
      res.send({ status: 200, data: response.data })
    })
    .catch((e) => {
      res.status(404)
      res.send({ state: 404, messages: e.message })
    })
})
module.exports = router
