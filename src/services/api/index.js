const { Router } = require('express')
const bodyParser = require('body-parser')
const catalog = require('./routes/catalog')

const router = Router()

router.use(bodyParser.json({ limit: '50mb' })) // for parsing application/json
router.use(bodyParser.urlencoded({ limit: '50mb', extended: true, parameterLimit: 50000 })) // for parsing application/x-www-form-urlencoded

router.use(catalog)

// Export the server middleware
module.exports = {
  path: '/api',
  handler: router
}
