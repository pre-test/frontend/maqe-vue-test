const Axios = require('axios')

const axios = Axios.create({
  baseURL: 'https://factools.qa.maqe.com/',
  headers: {
    'Content-Type': 'application/json; charset=utf-8',
    'Access-Control-Allow-Origin': '*'
  },
  retry: {
    retries: 3
  }
})

axios.interceptors.request.use((config) => {
    return config
  },
  (response) => {
    return response
  }
)

module.exports = axios
