export const state = () => {
  return {
    catalog: {
      category: [],
      brands: [],
      categoryAndBrandsId: {
        categoryId: [],
        brandsId: []
      }
    }
  }
}

export const actions = {
  // actions category
  setCategory({ commit }, category) {
    const newCategory = []
    commit('updateCategory', category);
    category.map((value) => {
      newCategory.push(value.id)
    })
    commit('updateCategoryIdList', newCategory)
  },

  setCategoryIdList({ commit }, index) {
    commit('deleteCategoryId', index)
  },

  // actions brands
  setBrands({ commit }, brands) {
    const brand = []
    commit('updateBrands', brands);
    brands.map((value) => {
      brand.push(value.id)
    })
    commit('updateBrandsIdList', brand)
  },

  // action clear filter
  setClearFilter({ commit }) {
    commit('deleteCategoryAndBrands')
  }
}

export const mutations = {
  // mutations category
  updateCategory(state, category) {
    state.catalog.category = category
  },

  // mutations brands
  updateBrands(state, brands) {
    state.catalog.brands = brands
  },

  // mutations updateCategoryAndBrands
  updateCategoryIdList(state, objectId) {
    state.catalog.categoryAndBrandsId.categoryId = objectId
  },
  updateBrandsIdList(state, objectId) {
    state.catalog.categoryAndBrandsId.brandsId = objectId
  },
  deleteCategoryId(state, index) {
    state.catalog.categoryAndBrandsId.categoryId.splice(index, 1)
    state.catalog.category.splice(index, 1)
  },

  // mutations clear all
  deleteCategoryAndBrands(state) {
    state.catalog.category.splice(0)
    state.catalog.brands.splice(0)
  }
}

export const getters = {
  'catalog': state => state.catalog
}
